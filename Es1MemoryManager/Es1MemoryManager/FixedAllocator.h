#pragma once
#include <vector>
#include "Chunk.h"
#include "Mallocator.h"

#ifndef DEFAULT_CHUNK_SIZE
#define DEFAULT_CHUNK_SIZE 4096
#endif

class  FixedAllocator {
private:
	std::size_t blockSize_;
	unsigned char numBlocks_;
	typedef std::vector<Chunk, Mallocator<Chunk>> Chunks;//same problem as in the creation of the vector in small obj class
	Chunks chunks_;
	Chunk* allocChunk_;  // alloc and dealloc chunck are used to speed up the proces they are used in from wich they get the name
	Chunk* deallocChunk_;//both are the last used to perform the action
	Chunk* emptyChunk_;
	mutable const FixedAllocator* prev_;
	mutable const FixedAllocator* next_;
public:
	void* Allocate();
	void  Deallocate(void* p);
	Chunk* VicinityFind(void* p) const;
	void  DoDeallocate(void* p);
	explicit FixedAllocator(std::size_t blockSize = 0);
	FixedAllocator(const FixedAllocator&);
	FixedAllocator& operator=(const FixedAllocator&);
	~FixedAllocator();
	void Swap(FixedAllocator& rhs);
	//it is used to get the size with wich the fixed allocator was created
	std::size_t BlockSize() const
	{
		return blockSize_;
	}
};
