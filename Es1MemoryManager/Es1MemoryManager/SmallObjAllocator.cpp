
#include "SmallObjAllocator.h"
#include <cassert>
#include <functional>

SmallObjAllocator::SmallObjAllocator(
	std::size_t chunkSize,
	std::size_t maxObjectSize)
	: pLastAlloc_(nullptr), pLastDealloc_(nullptr)
	, chunkSize_(chunkSize), maxObjectSize_(maxObjectSize)
{
}
namespace {
    struct CompareFixedAllocatorSize {//functor used to compare the seize used to decided which is the best Fixed to alloc the block
        bool operator()(const FixedAllocator& x, std::size_t numBytes) const
        {
            return x.BlockSize() < numBytes;
        }
    };
};
void* SmallObjAllocator::Allocate(std::size_t numBytes)
{
    if (pLastAlloc_ && pLastAlloc_->BlockSize() == numBytes)
    {
        return pLastAlloc_->Allocate();
    }
    //finds the first fixed that has a size big neuogh to contain my data
    Pool::iterator i = std::lower_bound(pool_.begin(), pool_.end(), numBytes, CompareFixedAllocatorSize());

    //if none is found a new one is created
    if (i == pool_.end() || i->BlockSize() != numBytes)
    {
        i = pool_.insert(i, FixedAllocator(numBytes));
        pLastDealloc_ = &*pool_.begin();
    }
    pLastAlloc_ = &*i;
    return pLastAlloc_->Allocate();
}

void SmallObjAllocator::Deallocate(void* p, std::size_t size)
{
    //check to see if something was already dealocated if yes we restart from there
    if (pLastDealloc_ && pLastDealloc_->BlockSize() == size)
    {
        pLastDealloc_->Deallocate(p);
        return;
    }
    Pool::iterator i = std::lower_bound(pool_.begin(), pool_.end(), size, CompareFixedAllocatorSize());
    assert(i != pool_.end());
    assert(i->BlockSize() == size);
    pLastDealloc_ = &*i;
    pLastDealloc_->Deallocate(p);
}

