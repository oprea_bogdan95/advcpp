#include "Chunk.h"
#include <cassert>

void Chunk::Init(std::size_t blockSize, unsigned char blocks)
{//pag 137
	assert(blockSize > 0);
	assert(blocks > 0);
	// Overflow check
	assert((blockSize * blocks) / blockSize == blocks);
	pData_ = static_cast<unsigned char*> (std::malloc(blockSize * blocks));//orinilay a new was used but had to change it after the creation of my memory manager
	Reset(blockSize, blocks);
}
void* Chunk::Allocate(std::size_t blockSize)
{/*The allocation function fetches the block indexed by firstAvailableBlock_ and
adjusts firstAvailableBlock_ to refer to the next available block�typical list
stuff. pag 138
*/
	if (!blocksAvailable_) return 0;
	unsigned char* pResult = pData_ + (firstAvailableBlock_ * blockSize);// pResult is given the next available block
	// Update firstAvailableBlock_ to point to the next block
	firstAvailableBlock_ = *pResult;
	--blocksAvailable_;
	return pResult;
}
void Chunk::Deallocate(void* p, std::size_t blockSize)
{/*The deallocation function does exactly the opposite: It passes the block back to the
free blocks list and increments blocksAvailable_. Don't forget that because Chunk
is agnostic regarding the block size, you must pass the size as a parameter to
Deallocate. pag 139*/
	assert(p >= pData_);//check to see if p in in the current chunk
	unsigned char* toRelease = static_cast<unsigned char*>(p);
	// Alignment check
	assert((toRelease - pData_) % blockSize == 0);
	*toRelease = firstAvailableBlock_;
	firstAvailableBlock_ = static_cast<unsigned char>((toRelease - pData_) / blockSize);
	// Truncation check
	assert(firstAvailableBlock_ == (toRelease - pData_) / blockSize);
	++blocksAvailable_;
}

void Chunk::Release()
{//the data managed by the chunk is relesed
	free(pData_);//changed from delete to free because now we use malloc
}

void Chunk::Reset(std::size_t blockSize, unsigned char blocks)
{//this function is used to clear a chunk that was already allocated, on mr Alexandresescu's book this is done directly in allocate
	assert(blockSize > 0);
	assert(blocks > 0);
	// Overflow check
	assert((blockSize * blocks) / blockSize == blocks);

	firstAvailableBlock_ = 0;
	blocksAvailable_ = blocks;

	unsigned char i = 0;
	unsigned char* p = pData_;
	for (; i != blocks; p += blockSize)
	{
		*p = ++i;
	}
}
