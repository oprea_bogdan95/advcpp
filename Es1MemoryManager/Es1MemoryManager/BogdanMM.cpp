#include "BogdanMM.h"

BogdanMM::BogdanMM():_chunkSize(0), _maxSize(0), ogAllocator(DEFAULT_CHUNK_SIZE, MAX_SMALL_OBJECT_SIZE)
{

}

void* BogdanMM::bogdan_new(std::size_t size)
{
	 return size > MAX_SMALL_OBJECT_SIZE ? ::operator new(size) : ogAllocator.Allocate(size);
}

void* BogdanMM::bogdan_new_a(std::size_t size, std::size_t numberOfElements)
{
	return size > MAX_SMALL_OBJECT_SIZE ? ::operator new[](size * numberOfElements) : ogAllocator.Allocate(size * numberOfElements);
}

void* BogdanMM::bogdan_malloc(std::size_t size)
{
	return size > MAX_SMALL_OBJECT_SIZE ? ::malloc(size) : ogAllocator.Allocate(size);
}

void BogdanMM::bogdan_delete(void* p, std::size_t size)
{
	size > MAX_SMALL_OBJECT_SIZE ? ::operator delete(p) : ogAllocator.Deallocate(p, size); 
}

void BogdanMM::bogdan_delete_a(void* p, std::size_t size, std::size_t numberOfElements)
{
	size > MAX_SMALL_OBJECT_SIZE ? ::operator delete[](p) : ogAllocator.Deallocate(p, size * numberOfElements);
}

void BogdanMM::bogdan_free(void* p, std::size_t size)
{
	size > MAX_SMALL_OBJECT_SIZE ? ::free(p) : ogAllocator.Deallocate(p, size);
}
