#pragma once
#include <vector>
#include "FixedAllocator.h"

class SmallObjAllocator {
private:
	typedef std::vector<FixedAllocator, Mallocator<FixedAllocator>> Pool;// after the creation of my memory manager there was the problem of the defoult allocator 
														//using operator new this generated an infinite loop so a new allocator had to be used one that used only malloc
	Pool pool_;
	FixedAllocator* pLastAlloc_;
	FixedAllocator* pLastDealloc_;
	std::size_t chunkSize_;
	std::size_t maxObjectSize_;
public:
	SmallObjAllocator(std::size_t chunkSize, std::size_t maxObjectSize);//chunck size is the defoult size of a chunk while the max is the value  to be considered small
	void* Allocate(std::size_t numBytes);
	void Deallocate(void* p, std::size_t size);
};
