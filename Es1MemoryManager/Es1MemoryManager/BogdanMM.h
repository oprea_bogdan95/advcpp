#pragma once

#define MM_NEW(SIZE) BogdanMM::getInstance().bogdan_new(SIZE)
#define MM_NEW_A(LENGHT, SIZE) BogdanMM::getInstance().bogdan_new_a(LENGHT*SIZE)
#define MM_MALLOC(SIZE) BogdanMM::getInstance().bogdan_malloc(SIZE)
#define MM_DELETE(POINTER, SIZE) BogdanMM::getInstance().bogdan_delete(POINTER,SIZE)
#define MM_DELETE_A(POINTER, LENGHT, SIZE) BogdanMM::getInstance().bogdan_delete_a(POINTER, LENGHT*SIZE)
#define MM_FREE(POINTER, SIZE) BogdanMM::getInstance().bogdan_free(POINTER,SIZE)

#include "SmallObjAllocator.h"

#ifndef MAX_SMALL_OBJECT_SIZE
#define MAX_SMALL_OBJECT_SIZE 64
#endif


class BogdanMM {
public:
	BogdanMM();

	static BogdanMM& getInstance()
	{
		static BogdanMM instance; // Guaranteed to be destroyed.
							  // Instantiated on first use.
		return instance;
	}
	BogdanMM(const BogdanMM&) = delete;
	BogdanMM& operator=(const BogdanMM&) = delete;
	void* bogdan_new(std::size_t size);
	void* bogdan_new_a(std::size_t size, std::size_t numberOfElements);
	void* bogdan_malloc(std::size_t size);
	void bogdan_delete(void* p, std::size_t size);
	void bogdan_delete_a(void* p, std::size_t size, std::size_t numberOfElements);
	void bogdan_free(void* p, std::size_t size);
private:
	std::size_t _chunkSize;
	std::size_t _maxSize;
	SmallObjAllocator ogAllocator;
};