// Es1MemoryManager.cpp : Questo file contiene la funzione 'main', in cui inizia e termina l'esecuzione del programma.
//

#include <iostream>
#include "SmallObjAllocator.h"
#include "BogdanMM.h"
#include <ctime>
#include <chrono>

using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::seconds;
using std::chrono::system_clock;
struct SmallObjTest {
    int v;
    long long a;
    char c;
    float b;

};
using namespace std;
void PerformanceTest(SmallObjAllocator& SmallObjAllocator)
{
    std::vector<void*> Pointers;
    auto start_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    for (int i = 0; i < 1000000; ++i)
    {
        void* ptr = MM_NEW(sizeof(SmallObjTest));
        Pointers.push_back(ptr);
    }
    for (int i = 0; i < 1000000; ++i)
    {
        MM_DELETE(Pointers[i], sizeof(SmallObjTest));
    }
    auto end_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    long long delta = end_millisec - start_millisec;
    cout << "SmallObjAllocator takes :" << std::to_string((float)delta / 1000) << " to complete" << endl; //convert to seconds

    std::vector<SmallObjTest*> Pointers2;

    start_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    for (int i = 0; i < 1000000; ++i)
    {
        SmallObjTest* ptr = new SmallObjTest();
        Pointers2.push_back(ptr);
    }
    for (int i = 0; i < 1000000; ++i)
    {
        delete Pointers2[i];
    }
    end_millisec = duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
    delta = end_millisec - start_millisec;
    cout << "Default Allocator takes :" << std::to_string((float)delta / 1000) << " to complete" << endl; //convert to seconds
}
int main() {
    size_t ChunkSize = 4;
    SmallObjAllocator SmallObjAllocator(4096, 64);
    PerformanceTest(SmallObjAllocator);
}
