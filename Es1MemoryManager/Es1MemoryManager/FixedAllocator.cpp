
#include "FixedAllocator.h"
#include <cassert>
#include <algorithm>


FixedAllocator::FixedAllocator(std::size_t blockSize) : blockSize_(blockSize)
, allocChunk_(nullptr)
, deallocChunk_(nullptr)
{
	//check to see if the block size if bigger then zero
	assert(blockSize_ > 0);

	prev_ = next_ = this;

	std::size_t numBlocks = DEFAULT_CHUNK_SIZE / blockSize;//finds how many block of cunks we can create based on the dimensions of the blok and chunk
	if (numBlocks > UCHAR_MAX) numBlocks = UCHAR_MAX;
	else if (numBlocks == 0) numBlocks = 8 * blockSize;

	numBlocks_ = static_cast<unsigned char>(numBlocks);
	assert(numBlocks_ == numBlocks);
}

FixedAllocator::FixedAllocator(const FixedAllocator& rhs) : blockSize_(rhs.blockSize_)
, numBlocks_(rhs.numBlocks_)
, chunks_(rhs.chunks_)
{
	prev_ = &rhs;
	next_ = rhs.next_;
	rhs.next_->prev_ = this;
	rhs.next_ = this;

	allocChunk_ = rhs.allocChunk_
		? &chunks_.front() + (rhs.allocChunk_ - &rhs.chunks_.front())
		: 0;

	deallocChunk_ = rhs.deallocChunk_
		? &chunks_.front() + (rhs.deallocChunk_ - &rhs.chunks_.front())
		: 0;
}


FixedAllocator& FixedAllocator::operator=(const FixedAllocator& rhs)
{
	FixedAllocator copy(rhs);
	copy.Swap(*this);
	return *this;
}


FixedAllocator::~FixedAllocator()
{
	if (prev_ != this)
	{
		prev_->next_ = next_;
		next_->prev_ = prev_;
		return;
	}

	assert(prev_ == next_);
	//release of the memory managed by the cunks inside the allocator
	Chunks::iterator i = chunks_.begin();
	for (; i != chunks_.end(); ++i)
	{
		assert(i->blocksAvailable_ == numBlocks_);
		i->Release();
	}
}

void FixedAllocator::Swap(FixedAllocator& rhs)
{
	using namespace std;

	swap(blockSize_, rhs.blockSize_);
	swap(numBlocks_, rhs.numBlocks_);
	chunks_.swap(rhs.chunks_);
	swap(allocChunk_, rhs.allocChunk_);
	swap(deallocChunk_, rhs.deallocChunk_);
}

void* FixedAllocator::Allocate()
{//pag 140
	if (allocChunk_ == 0 || allocChunk_->blocksAvailable_ == 0)
	{
		Chunks::iterator i = chunks_.begin();
		for (;; ++i)
		{
			if (i == chunks_.end())
			{
				chunks_.reserve(chunks_.size() + 1);
				Chunk newChunk;
				newChunk.Init(blockSize_, numBlocks_);
				chunks_.push_back(newChunk);
				allocChunk_ = &chunks_.back();
				deallocChunk_ = &chunks_.back();
				break;
			}
			if (i->blocksAvailable_ > 0)
			{
				allocChunk_ = &*i;
				break;
			}
		}
	}
	assert(allocChunk_ != 0);
	assert(allocChunk_->blocksAvailable_ > 0);
	return allocChunk_->Allocate(blockSize_);
}

void FixedAllocator::Deallocate(void* p)
{
	assert(!chunks_.empty());
	assert(&chunks_.front() <= deallocChunk_);
	assert(&chunks_.back() >= deallocChunk_);

	deallocChunk_ = VicinityFind(p);
	assert(deallocChunk_);

	DoDeallocate(p);
}

Chunk* FixedAllocator::VicinityFind(void* p) const {
	if (chunks_.empty()) return nullptr;
	assert(deallocChunk_);

	const std::size_t chunkLength = numBlocks_ * blockSize_;
	Chunk* lo = deallocChunk_;
	Chunk* hi = deallocChunk_ + 1;
	const Chunk* loBound = &chunks_.front();//finds ne
	const Chunk* hiBound = &chunks_.back() + 1;

	// Special case: deallocChunk_ is the last in the array
	if (hi == hiBound) hi = nullptr; // boundary condition

	for (;;)
	{//check if p is between the begining and the end meaning this is the correct posistion 
		if (lo)
		{
			if (p >= lo->pData_ && p < lo->pData_ + chunkLength) { 
				return lo; 
			} 
			if (lo == loBound)
			{
				lo = nullptr;//we arrived at the begining 
			}
			else --lo;//decrement until we find the correct position
		}

		if (hi)
		{
			if (p >= hi->pData_ && p < hi->pData_ + chunkLength) {
				return hi;
			}
			if (++hi == hiBound)
			{
				hi = nullptr;
			}
		}
	}

	return nullptr;

}

void FixedAllocator::DoDeallocate(void* p)
{
	assert(deallocChunk_->pData_ <= p);
	assert(deallocChunk_->pData_ + numBlocks_ * blockSize_ > p);

	// call into the chunk, will adjust the inner list but won't release memory
	deallocChunk_->Deallocate(p, blockSize_);


	if (deallocChunk_->blocksAvailable_ == numBlocks_) 
	{
		assert(emptyChunk_ != deallocChunk_);
		// deallocChunk_ is empty, but a Chunk is only released if there are 2
		// empty chunks.  Since emptyChunk_ may only point to a previously
		// cleared Chunk, if it points to something else besides deallocChunk_,
		// then FixedAllocator currently has 2 empty Chunks. So this means we can dealocate 
		Chunk* lastChunk = &chunks_.back();
		if (lastChunk == deallocChunk_)
		{
			// check if we have two last chunks that are empty

			if (chunks_.size() > 1 &&
				deallocChunk_[-1].blocksAvailable_ == numBlocks_)
			{
				// There are 2 free chunks, so we can release the last one
				lastChunk->Release();
				chunks_.pop_back();
				allocChunk_ = deallocChunk_ = &chunks_.front();
			}
			return;
		}

		if (lastChunk->blocksAvailable_ == numBlocks_)
		{
			// Two free blocks, we can discard the last one
			lastChunk->Release();
			chunks_.pop_back();
			allocChunk_ = deallocChunk_;
		}
		else
		{
			// move the empty chunk to the end
			std::swap(*deallocChunk_, *lastChunk);
			allocChunk_ = &chunks_.back();
		}
	}
}