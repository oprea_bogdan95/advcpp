#pragma once
#include <iostream>

class Chunk {//pag 136 of the Book made by mr Alexandrescu
public:
	void Init(std::size_t blockSize, unsigned char blocks);
	void* Allocate(std::size_t blockSize);
	void Deallocate(void* p, std::size_t blockSize);
	void Release();
	void Reset(std::size_t blockSize, unsigned char blocks);
	unsigned char* pData_;
	unsigned char firstAvailableBlock_, blocksAvailable_;
	/*fAB holds the index of the first block available in
this chunk, while bA  the number of blocks available in this chunk*/

};
