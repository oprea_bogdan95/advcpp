#pragma once
#include <iterator>  

template<class U>
class SList {

public:
	using value_type = U;
	using size_type = std::size_t;
	using reference = typename value_type&;
private:
	struct Node
	{
		value_type value;
		Node* next;
		Node(): next(nullptr) {}
		Node(value_type val, Node* nxt) :value(val), next(nxt) {}
		virtual ~Node() {
			if (next)
			{
				delete next;
			}
		};
	};

public:
	template < typename T >
	// Dichiarazione della classe iterator della lista
	struct SListIterator
	{
	private:
		Node* _head;
	public:
		friend class SList;//cosi che la classe lista possa accedere ai mebri privati del iteraror

		//tags sono usati per selezionare l'algoritmo pi� efficiente se il contenitore viene passato a una delle funzioni della libreria standard dalla libreria <algorithm>
		//inoltre i tags sono una cosa che si deve sempre aggiungere in quanto ci si aspetta che l'iteratore gli abbia
		using iterator_category = std::forward_iterator_tag;
		using difference_type = std::ptrdiff_t;//tipo intero con segno che pu� essere utilizzato per identificare la distanza tra i passaggi dell'iteratore 
		using value_type = SList::value_type;
		using pointer = typename value_type*;
		using reference = typename value_type&;

		SListIterator() :_head(nullptr) {}
		SListIterator(Node* x) :_head(x) {}
		SListIterator(const SListIterator& mit) : _head(mit._head) {}

		SListIterator& operator++() {//prefix
			if(_head){
				_head = _head->next;
			}
			return *this;
		}
		SListIterator operator++(int) {//postfix
			SListIterator tmp = *this;
			++(*this);
			return tmp;
		}

		bool operator==(const SListIterator& rhs) const {
			return _head == rhs._head;
		}
		bool operator!=(const SListIterator& rhs) const {
			return _head != rhs._head;
		}
		
		reference operator*() {
			return (*_head).value;
		}

		pointer operator->() {
			return &(_head->value);
		}

		virtual ~SListIterator() {};

	};

	using iterator = SListIterator<value_type>;//per rispettare la firma della stl list
	using const_iterator = SListIterator<const value_type>;//per rispettare la firma della stl list
private:
	iterator iter;
	const_iterator citer;
public:
	//Costruttori
	//default (1)costruttore di contenitori vuoti (costruttore predefinito) Costruisce un contenitore vuoto, senza elementi.
	explicit SList();
	explicit SList(size_type n);
	//fill(2)Costruisce un contenitore con n elementi. Ogni elemento � una copia di val. 
	explicit SList(size_type n, const value_type& val);
	//range(3)Costruisce un contenitore con tanti elementi quanti sono l'intervallo [first,last), con ogni elemento costruito 
	//dall'elemento corrispondente in quell'intervallo, nello stesso ordine.
	template <class InputIterator>
	SList(InputIterator first, InputIterator last);
	//copy(4)Costruisce un contenitore con una copia di ciascuno degli elementi in x, nello stesso ordine. 
	SList(const SList& x);
	//move (5)Costruisce un contenitore che acquisisce gli elementi di list
	SList(SList&& list);
	//initializer list (6)
	SList(std::initializer_list<value_type> il);
	//Distruttore
	virtual ~SList();

	SList& operator= (const SList& fwdlst);
	SList& operator=(SList&& other);

	//Iteratori
	iterator begin()noexcept;
	iterator end() noexcept;
	const_iterator cbegin() const noexcept;
	const_iterator cend() const noexcept;
	iterator before_begin() noexcept;
	const_iterator cbefore_begin() const noexcept;
	void push_front(const value_type& val);
	void pop_front();

	reference front();
	
	iterator insert_after(const_iterator position, const value_type& val);
	iterator insert_after(const_iterator position, value_type&& val);
	iterator insert_after(const_iterator position, size_type n, const value_type& val);
	template <class InputIterator>
	iterator insert_after(const_iterator position, InputIterator first, InputIterator last);
	iterator insert_after(const_iterator position, std::initializer_list<value_type> il);
	iterator erase_after(const_iterator position);
	iterator erase_after(const_iterator position, const_iterator last);
	bool empty() const noexcept;

	void clear() noexcept;
	
	void swap(SList& list);
};

template<class U>
inline SList<U>::SList() : iter(new Node())
{
}

template<class U>
inline SList<U>::SList(size_type n) : SList()
{
	insert_after(before_begin(), n, value_type());
}
template<class U>
inline SList<U>::SList(size_type n, const value_type& val) : SList()
{
	insert_after(before_begin(), n, val);
}
template<class U>
template<class InputIterator>
inline SList<U>::SList(InputIterator first, InputIterator last) : SList()
{
	insert_after(cbefore_begin(), first, last);
}
template<class U>
inline SList<U>::SList(const SList& x) : SList()
{
	insert_after(cbefore_begin(), x.cbegin(), x.cend());
}

template<class U>
inline SList<U>::SList(SList&& list): iter()
{
	std::swap(iter, list.iter);
}

template<class U>
inline SList<U>::SList(std::initializer_list<value_type> il) : SList()
{
	insert_after(cbefore_begin(), il);
}

template<class U>
inline SList<U>::~SList()
{
	if (!iter._head || iter._head->next == nullptr) return;

	delete iter._head;
}

template<class U>
inline typename SList<U>& SList<U>::operator=(const SList& other)
{
	if (&other != this) {
		SList<U> tmp(other);
		this->swap(tmp);
	}
	return *this;
}

template<class U>
inline typename SList<U>& SList<U>::operator=(SList&& other)
{
	if (this != &other) {
		std::swap(iter, other.iter); 
	}
	return *this;
}

template<class U>
inline typename SList<U>::iterator SList<U>::begin() noexcept
{
	iterator begin = std::next(iter);
	return begin;
}

template<class U>
inline typename SList<U>::iterator SList<U>::end() noexcept
{
	return nullptr;
}

template<class U>
inline typename SList<U>::const_iterator SList<U>::cbegin() const noexcept
{
	iterator begin = std::next(iter);
	return const_iterator(begin._head);
}

template<class U>
inline typename SList<U>::const_iterator SList<U>::cend() const noexcept
{
	return nullptr;
}

template<class U>
inline typename SList<U>::iterator SList<U>::before_begin() noexcept
{
	return iter;
}

template<class U>
inline typename SList<U>::const_iterator SList<U>::cbefore_begin() const noexcept
{
	return const_iterator(iter._head);
}

template<class U>
inline void SList<U>::push_front(const value_type& val)
{
	insert_after(cbefore_begin(), val);
}

template<class U>
inline void SList<U>::pop_front()
{
	erase_after(cbefore_begin());
}

template<class U>
inline typename SList<U>::reference SList<U>::front()
{
	return *begin();
}

template<class U>
inline typename SList<U>::iterator SList<U>::insert_after(const_iterator position, const value_type& val)
{

	iterator previousIterator(position._head);
	const_iterator next = std::next(position);

	Node* newNode = new Node(val, next._head);
	previousIterator._head->next = newNode;

	return iterator(newNode);
}

template<class U>
inline typename SList<U>::iterator SList<U>::insert_after(const_iterator position, value_type&& val)
{//il && indicauna r-value reference

	iterator previousIterator = iterator(position._head);
	const_iterator next = std::next(position);

	Node* newNode = new Node();
	newNode->value = std::move(val);
	newNode->next = next._head;

	previousIterator._head->next = newNode;

	return iterator(newNode);
}

template<class U>
inline typename SList<U>::iterator SList<U>::insert_after(const_iterator position, size_type n, const value_type& val)
{

	for (size_type i = 0; i < n; ++i, ++position)
	{
		insert_after(position, val);
	}

	return iterator(position._head);
}

template<class U>
template<class InputIterator>
inline typename SList<U>::iterator SList<U>::insert_after(const_iterator position, InputIterator first, InputIterator last)
{
	while (first != last) {
		insert_after(position, *first);
		++first; 
		++position;
	}
	return iterator(position._head);
}
template<class U>
inline typename SList<U>::iterator SList<U>::insert_after(const_iterator position, std::initializer_list<value_type> il)
{
	return insert_after(position, il.begin(), il.end());
}
template<class U>
inline typename SList<U>::iterator SList<U>::erase_after(const_iterator position)
{	
	const_iterator previousIterator = position;
	const_iterator it = std::next(position);
	if (it._head != nullptr)
	{
		previousIterator._head->next = it._head->next;
		Node* temp = it._head;

		++it;

		temp->next = nullptr;
		delete temp;
	}

	return iterator(it._head);
}
template<class U>
inline typename SList<U>::iterator SList<U>::erase_after(const_iterator position, const_iterator last)
{
	iterator eraseResult(position._head);
	iterator lastIT(last._head);
	
	while (eraseResult != lastIT) {
		eraseResult = erase_after(position);
	}
	return eraseResult;
}

template<class U>
inline bool SList<U>::empty() const noexcept
{
	return cbegin() == cend();
}

template<class U>
inline void SList<U>::clear() noexcept
{
	erase_after(cbefore_begin(), cend());
}

template<class U>
inline void SList<U>::swap(SList& list)
{
	std::swap(iter, list.iter);
}
