// Es2Lists.cpp : Questo file contiene la funzione 'main', in cui inizia e termina l'esecuzione del programma.
//

#include <iostream>
#include "SList.h"

using std::cout;
using std::endl;

template <class ForwardIterator>
void print(ForwardIterator begin, ForwardIterator end)
{
    while (begin != end)
    {
        cout << *begin++ << " ";
    }

    cout << endl;
}
int main()
{   
    //test costruttore di default
    SList<int> l;
    print(l.cbegin(), l.cend());

   //test empty
    bool IsEmpty = l.empty();
    bool exptectedResult = true;
    cout << "Risultato: " << (IsEmpty == exptectedResult ? "Lista vuota" : "Lista non vuota") << endl;
    cout << "Risultato aspettato: " << (exptectedResult ? "Lista vuota" : "Lista non vuota") << endl;
    
    //test push
    l.push_front(4);
    l.push_front(1);
    l.push_front(2);
    l.push_front(3);
    print(l.cbegin(), l.cend());
    //test insert after
    int c = 7;
    l.insert_after(l.cbegin(), c);
    cout << "Lista 1 dopo inserimento dopo l'inizio" << endl;
    print(l.cbegin(), l.cend());

    //test copy costructor
    SList<int> l2 = l;
    cout << "Lista 2 copy costructor" << endl;
    print(l2.cbegin(), l2.cend());
    cout << "Front di lista numero 2: " << l2.front() << endl;

    //test pop front and front
    l2.pop_front();
    cout << "Front di lista numero 2 dopo pop_front: " << l2.front() << endl;
    print(l2.cbegin(), l2.cend());
   
    //test move
    SList<int> l3 = std::move(l2);
    cout << "Lista numero 3 costruita usando std::move con lista 2: "<< endl;
    print(l3.cbegin(), l3.cend());
    cout << "Lista numero 2 dopo il move:" << endl;
    print(l2.cbegin(), l2.cend());

    // test initializer list
    SList<int> l4{ 10, 20, 30, 40, 50, 60 };
    cout << "Lista 4 creata con inizializer list" << endl;
    print(l4.cbegin(), l4.cend());

    //test swap
    l4.swap(l);
    cout << "Lista 4 dopo swap con lista 1" << endl;
    print(l4.cbegin(), l4.cend());
    cout << "Lista 1 dopo swap con lista 4" << endl;
    print(l.cbegin(), l.cend());

    //test clear
    l4.clear();
    bool IsEmptyList = l4.empty();
    bool exptected = true;
    cout << "Lista 4 dopo clear:" << endl;
    cout << "Risultato: " << (IsEmptyList == exptected ? "Lista vuota" : "Lista non vuota") << endl;
    cout << "Risultato aspettato: " << (exptected ? "Lista vuota" : "Lista non vuota") << endl;

    //test push front
    int inFront = 9;
    l.push_front(inFront);
    cout << "Lista 1 dopo push_front" << endl;
    print(l.cbegin(), l.cend());
    SList<int> l5;
    l5 = std::move(l);
    cout << "Lista numero 5 inizializata con la lista numero 1 usando il move assignment " << endl;
    print(l5.cbegin(), l5.cend());
    cout << "Lista numero 1 dopo il move:" << endl;
    print(l.cbegin(), l.cend());
}
